package clevertec.java.collection;

import java.util.Iterator;

public class SimpleArrayDeque<E> implements InterfaceDequeDemo<E> {

    private E[] values;
    private int size;
    private int capacity;
    private static final int DEFAULT_CAPACITY = 10;

    public SimpleArrayDeque() throws ClassCastException{
        values = (E[]) new Object[DEFAULT_CAPACITY];
        capacity = DEFAULT_CAPACITY;
        size = 0;
    }

    public SimpleArrayDeque(E[] values) {
        this.values = values;
        this.size = values.length;
        capacity = values.length;
    }

    public SimpleArrayDeque(int initialCapacity) {
        if (initialCapacity < 0){
            throw  new IllegalArgumentException("Intial capacity cannot be less than 0");
        }
        values = (E[]) new Object[DEFAULT_CAPACITY];
        capacity = initialCapacity;
        size = 0;
    }

    @Override
    public boolean addFirst(E e) {
        try {
            increaseCapacity(size + 1);
            E[] temp = values;
            values = (E[]) new Object[capacity];
            values[0] = e;
            System.arraycopy(temp, 0, values, 1, size);
            size++;
            return true;
        } catch (ClassCastException exception){
            exception.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean addLast(E e) {
        try {
            increaseCapacity(size + 1);
            E[] temp = values;
            values = (E[]) new Object[capacity];
            System.arraycopy(temp, 0, values, 0, size);
            values[size] = e;
            size++;
            return true;
        } catch (ClassCastException exception){
            exception.printStackTrace();
        }
        return false;
    }

    @Override
    public E removeFirst() {
        E temp = values[0];
        size--;
        System.arraycopy(values, 1, values, 0, size);
        return temp;
    }

    @Override
    public E removeLast() {
        E temp = values[size - 1];
        size--;
        return temp;
    }

    @Override
    public E getFirst() {
        return values[0];
    }

    @Override
    public E getLast() {
        return values[size - 1];
    }

    @Override
    public boolean removeALL() {
        SimpleIteratorArrayDeque<E> simpleIteratorArrayDeque = new SimpleIteratorArrayDeque<E>(values, size);
        boolean modified = false;
        while (simpleIteratorArrayDeque.hasNext()){
            removeLast();
            simpleIteratorArrayDeque.next();
            modified = true;
        }
        return modified;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    public void increaseCapacity(int lengthAfterAdding) {
        if (lengthAfterAdding > capacity) {
            capacity = (capacity*3)/2 + 1;
        }
    }

    @Override
    public String toString(){
        SimpleIteratorArrayDeque<E> simpleIteratorArrayDeque = new SimpleIteratorArrayDeque<E>(values, size);
        StringBuilder str = new StringBuilder();
        while (simpleIteratorArrayDeque.hasNext()){
            str.append(simpleIteratorArrayDeque.next());
            str.append("\t");
        }
        return str.toString();
    }

    @Override
    public Iterator iterator() {
        return new SimpleIteratorArrayDeque(values, size);
    }

    public int getCapacity() {
        return capacity;
    }

    public int getSize() {
        return size;
    }
}

