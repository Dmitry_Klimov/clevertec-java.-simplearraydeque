package clevertec.java.collection;

import java.util.Iterator;

public class SimpleIteratorArrayDeque<T> implements Iterator<T> {

    private T[] objects;
    private int size;
    private int index = 0;

    public SimpleIteratorArrayDeque(T[] objects, int size) {
        this.objects = objects;
        this.size = size;
    }

    @Override
    public boolean hasNext() {
        return index < size;
    }

    @Override
    public T next() {
        return objects[index++];
    }
}