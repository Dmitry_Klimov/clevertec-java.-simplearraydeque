package clevertec.java.collection;

public interface InterfaceDequeDemo<E> extends Iterable<E>{
    boolean addFirst(E e);
    boolean addLast(E e);
    E removeFirst();
    E removeLast();
    E getFirst();
    E getLast();
    boolean removeALL();
    int size();
    boolean isEmpty();
}
