package clevertec.java.collection;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class SimpleArrayDequeTest {

    private final Integer[] masForTest = {5,7,2,9,1,3,8,2};
    private final SimpleArrayDeque arrayDequeNonEmpty = new SimpleArrayDeque(masForTest);
    private final SimpleArrayDeque arrayDequeEmptyWithCapacityZero = new SimpleArrayDeque(0);

    @Test(expected = IllegalArgumentException.class)
    public void  whenCreateSimpleArrayDequeWithNegativeCapacityThenGetIllegalArgumentException(){
        SimpleArrayDeque simpleArrayDeque = new SimpleArrayDeque(-1);
    }

    @Test
    public void mustReturnZeroSizeWhenMapIsNew() {
        SimpleArrayDeque simpleArrayDeque = new SimpleArrayDeque();
        final int result = simpleArrayDeque.getSize();
        assertThat(result, is(0));
    }

    @Test
    public void whenAddFirstThenReturnTrue() {
        final boolean result = arrayDequeEmptyWithCapacityZero.addFirst(1);
        assertTrue(result);
    }

    @Test
    public void whenAddLastThenReturnTrue() {
        final boolean result = arrayDequeEmptyWithCapacityZero.addFirst(1);
        assertTrue(result);
    }

    @Test
    public void whenRemoveFirstThenReturnFirstElement() {
        final Integer result = (Integer) arrayDequeNonEmpty.removeFirst();
        assertThat(result, is(5));
    }

    @Test
    public void whenRemoveFirstThenAlwaysReturnFirstElement() {
        arrayDequeNonEmpty.removeFirst();
        arrayDequeNonEmpty.removeFirst();
        arrayDequeNonEmpty.removeFirst();
        final Integer result = (Integer) arrayDequeNonEmpty.removeFirst();
        assertThat(result, is(9));
    }

    @Test
    public void whenRemoveLastThenReturnLastElement() {
        final Integer result = (Integer) arrayDequeNonEmpty.removeLast();
        assertThat(result, is(2));
    }

    @Test
    public void whenRemoveLastThenAlwaysReturnLastElement() {
        arrayDequeNonEmpty.removeLast();
        arrayDequeNonEmpty.removeLast();
        arrayDequeNonEmpty.removeLast();
        final Integer result = (Integer) arrayDequeNonEmpty.removeLast();
        assertThat(result, is(1));
    }

    @Test
    public void whenGetFirstThenReturnFirstElement() {
        final Integer result = (Integer) arrayDequeNonEmpty.getFirst();
        assertThat(result, is(5));
    }

    @Test
    public void whenGetLastThenReturnLastElement() {
        final Integer result = (Integer) arrayDequeNonEmpty.getLast();
        assertThat(result, is(2));
    }

    @Test
    public void whenRemoveALLThenReturnTrue() {
        final boolean result = arrayDequeNonEmpty.removeALL();
        assertTrue(result);
    }

    @Test
    public void whenSizeThenReturnSizeCollection() {
        final int result = arrayDequeNonEmpty.size();
        assertThat(result, is(8));
    }

    @Test
    public void whenIsEmptyForNonEmptyCollectionThenReturnFalse() {
        final boolean result = arrayDequeNonEmpty.isEmpty();
        assertFalse(result);
    }

    @Test
    public void whenIsEmptyForEmptyCollectionThenReturnTrue() {
        final boolean result = arrayDequeEmptyWithCapacityZero.isEmpty();
        assertTrue(result);
    }

    @Test
    public void ifLengthAfterAddingMoreCapacityWhenIncreaseCapacityThenIncreaseCapacityAccordingFormula() {
        arrayDequeEmptyWithCapacityZero.addLast(1);
        arrayDequeEmptyWithCapacityZero.addLast(1);
        arrayDequeEmptyWithCapacityZero.addLast(1);
        arrayDequeEmptyWithCapacityZero.addLast(1);
        arrayDequeEmptyWithCapacityZero.addLast(1);
        final int result = arrayDequeEmptyWithCapacityZero.getCapacity();
        assertThat(result, is(7));
    }
}